{
    'name': 'Job Application Module',
    'author': 'Abdelfattah Mohamed',
    'depends': [
        'base',
        'website',
    ],

    'data': [
        'views/job_application_view.xml',
        'security/ir.model.access.csv',
        'security/x_custom_application_data.xml',
        'report/app_report.xml',
        'report/report.xml',
    ],
}
