import base64
from odoo import http
from odoo.http import request
from datetime import datetime


class WebsiteJobApplication(http.Controller):

    @http.route('/job-application-form', type="http", methods=["POST", "GET"], auth='public', website=True)
    def job_application_form(self, **kwargs):

        job_application = request.env['job.application']
        state = job_application.fields_get(['state'])['state']['selection']

        custom_fields = {}
        excluded_fields = ['x_', 'date_', 'create_date', 'write_date', 'id', '__last_update', 'display_name',
                           'create_uid', 'write_uid', 'applied_date','state']

        for field_name, field_info in job_application.fields_get().items():
            if field_name not in excluded_fields:
                custom_fields[field_name] = field_info

        field_html = ''
        for field_name, field_info in custom_fields.items():
            label = field_info['string']
            ftype = field_info['help']
            field_html += f'<div style="margin-bottom: 10px;">'
            field_html += f'<label style="display: inline-block; width: 150px; text-align: right; margin-right: 10px;" for="{field_name}">{label}:</label>'
            field_html += f'<input style="display: inline-block; width: 300px;" type="{ftype}" name="{field_name}" />'
            field_html += f'</div>'

        if request.httprequest.method == "POST":

            error_list = []
            if not kwargs.get('name'):
                error_list.append("The name field is mandatory")

            if not kwargs.get('email'):
                error_list.append("The email field is mandatory")

            if kwargs.get('phone'):
                if not kwargs.get('phone').isdigit():
                    error_list.append("Phone number must contain only digits")

            if kwargs.get('date_of_birth'):
                date_obj = datetime.strptime(kwargs.get('date_of_birth'), '%Y-%m-%d')
                today = datetime.now().date()
                age = today.year - date_obj.year - (
                        (today.month, today.day) < (date_obj.month, date_obj.day))
                if age < 20:
                    error_list.append("Age must be grater than 20")

            if error_list:

                field_html = ''
                for field_name, field_info in custom_fields.items():
                    label = field_info['string']
                    ftype = field_info['help']
                    field_html += f'<div style="margin-bottom: 10px;">'
                    field_html += f'<label style="display: inline-block; width: 150px; text-align: right; margin-right: 10px;" for="{field_name}">{label}:</label>'
                    field_html += f'<input style="display: inline-block; width: 300px;" type="{ftype}" value="{kwargs.get(field_name)}" name="{field_name}" />'
                    field_html += f'</div>'

                return request.render('x_custom_application.view_job_application_form_website', {
                    'error_list': error_list, 'field_html': field_html,'state': state,
                })
            else:
                new_data = {}
                for field_name, field_value in kwargs.items():
                    if field_value:
                        new_data[field_name] = field_value
                        print(new_data)

                uploaded_file = request.httprequest.files['resume']
                file = uploaded_file.read()
                new_data['resume'] = base64.b64encode(file)

                request.env['job.application'].create(new_data)

                return request.render('x_custom_application.view_job_application_form_website', {
                    'success': 'Thank you for submitting your request',
                })

        return request.render('x_custom_application.view_job_application_form_website', {
            'field_html': field_html,'state': state,
        })
