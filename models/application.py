from odoo import models, fields, api
from odoo.exceptions import ValidationError
from datetime import datetime


class JobApplication(models.Model):
    _name = 'job.application'
    _description = 'Job Application'

    name = fields.Char(string='Name', help="text ^[a-zA-Z]+$", required=True)
    email = fields.Char(string='Email', help="email", required=True)
    phone = fields.Char(string='Phone', help="tel")
    state = fields.Selection([('cairo', 'Cairo'), ('giza', 'Giza'), ('alexandria', 'Alexandria')], string='state',
                             help="")
    date_of_birth = fields.Date(string='Date of Birth', help="date")
    resume = fields.Binary(string='Resume', help="file")
    applied_date = fields.Date(string='Applied Date', default=fields.Date.today(), readonly=True, help="date")

    @api.constrains('phone')
    def _check_phone(self):
        for record in self:
            if record.phone and not record.phone.isdigit():
                raise ValidationError('Phone number must contain only digits.')

    @api.constrains('date_of_birth')
    def _check_min_age(self):
        for record in self:
            if record.date_of_birth:
                today = datetime.now().date()
                age = today.year - record.date_of_birth.year - (
                        (today.month, today.day) < (record.date_of_birth.month, record.date_of_birth.day))
                if age < 20:
                    raise ValidationError('Age must be grater than 20.')

    @api.constrains('resume')
    def _check_resume_size(self):
        for record in self:
            if record.resume and len(record.resume) / (1024 * 1024) > 5:  # Limit to 5MB
                raise ValidationError('Resume file size should not exceed 5MB.')

    # @api.model
    # def create_record_rules(self):
    #     self.env['ir.rule'].create({
    #         'name': 'Cairo Admin Job Applications',
    #         'model_id': self.env.ref('x_custom_application.model_job_application').id,
    #         'domain_force': "[('state', '=', 'Cairo')]",
    #         'groups': [(4, self.env.ref('x_custom_application.group_cairo_admin').id)],
    #         'perm_read': True,
    #         'perm_write': True,
    #         'perm_create': True,
    #     })
    #
    #     # Create a Record Rule for Admin 2 (Giza)
    #     self.env['ir.rule'].create({
    #         'name': 'Giza Admin Job Applications',
    #         'model_id': self.env.ref('x_custom_application.model_job_application').id,
    #         'domain_force': "[('state', '=', 'giza')]",
    #         'groups': [(4, self.env.ref('x_custom_application.group_giza_admin').id)],
    #         'perm_read': True,
    #         'perm_write': True,
    #         'perm_create': True,
    #     })
    #
    # #
    # @api.model
    # def init(self):
    #     # Call the function or execute the code you want to run after module installation/upgrade
    #     print("here")
    #     self.create_record_rules()


# class JobApplicationReport(models.AbstractModel):
#     _name = 'x_custom_application.report_job_application'
#     _description = 'Job Application Report'
#     print("Good")
#
#     @api.model
#     def _get_report_values(self, docids, data=None):
#         # Fetch the data needed for the report (e.g., job applications)
#         records = self.env['job.application'].browse(docids)
#         print("hi")
#         print(records)
#         return {
#             'doc_ids': docids,
#             'doc_model': 'job.application',
#             'docs': records,
#         }
